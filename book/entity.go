package book

import "gopkg.in/mgo.v2/bson"

type Book struct {
	Id          bson.ObjectId `json:"id" bson:"_id"`
	Title       string        `json:"title" bson:"title"`
	Description string        `json:"description" bson:"description"`
	Price       int           `json:"price" bson:"price"`
	Author      string        `json:"author" bson:"author"`
}
