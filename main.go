package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/elyakimnf/books-api/book"
	"github.com/elyakimnf/books-api/db"
	"github.com/elyakimnf/books-api/model"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func main() {

	r := mux.NewRouter()
	r.HandleFunc("/books", FindAllBook).Methods("GET")
	r.HandleFunc("/books/{id}", FindBookByID).Methods("GET")
	r.HandleFunc("/books/create", CreateBook).Methods("POST")
	r.HandleFunc("/books/update", UpdateBook).Methods("PUT")
	r.HandleFunc("/books/delete/{id}", DeleteBook).Methods("DELETE")

	err := http.ListenAndServe(":8070", r)
	if err != nil {
		fmt.Println(err)
	}

	// if connectToMongo() {
	// 	fmt.Println("Connected")
	// } else {
	// 	fmt.Println("Not Connected")
	// }

}

// func connectToMongo() bool {
// 	ret := false
// 	fmt.Println("enter main - connecting to mongo")

// 	// tried doing this - doesn't work as intended
// 	defer func() {
// 		if r := recover(); r != nil {
// 			fmt.Println("Detected panic")
// 			var ok bool
// 			err, ok := r.(error)
// 			if !ok {
// 				fmt.Printf("pkg:  %v,  error: %s", r, err)
// 			}
// 		}
// 	}()

// 	maxWait := time.Duration(5 * time.Second)
// 	session, sessionErr := mgo.DialWithTimeout("elcluster-shard-00-01.gwk0h.mongodb.net:27017", maxWait)
// 	if sessionErr == nil {
// 		session.SetMode(mgo.Monotonic, true)
// 		coll := session.DB("book-api").C("books")
// 		if coll != nil {
// 			fmt.Println("Got a collection object")
// 			ret = true
// 		}
// 	} else { // never gets here
// 		fmt.Println("Unable to connect to local mongo instance!")
// 	}
// 	return ret

func CreateBook(w http.ResponseWriter, r *http.Request) {
	db, err := db.GetMongoDB()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	} else {
		bookModel := model.BookModel{
			Db:         db,
			Collection: "books",
		}
		var book book.Book
		book.Id = bson.NewObjectId()
		err2 := json.NewDecoder(r.Body).Decode(&book)
		// Session.DB("CRUD").Collection("crud book").Insert(book)
		if err2 != nil {
			respondWithError(w, http.StatusBadRequest, err2.Error())
			return
		} else {
			err3 := bookModel.Create(&book)
			if err3 != nil {
				respondWithError(w, http.StatusBadRequest, err3.Error())
				return
				// respondWithjson(w, http.StatusOK, book)
			} else {
				respondWithJson(w, http.StatusOK, book)
			}
		}
	}
}

func UpdateBook(w http.ResponseWriter, r *http.Request) {
	db, err := db.GetMongoDB()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	} else {
		bookModel := model.BookModel{
			Db:         db,
			Collection: "books",
		}
		var book book.Book
		err2 := json.NewDecoder(r.Body).Decode(&book)
		if err2 != nil {
			respondWithError(w, http.StatusBadRequest, err2.Error())
			return
		} else {
			err3 := bookModel.Update(&book)
			if err3 != nil {
				respondWithError(w, http.StatusBadRequest, err3.Error())
				return
			} else {
				respondWithJson(w, http.StatusOK, book)
			}
			// respondWithjson(w, http.StatusOK, book)

		}
	}
}

func FindBookByID(w http.ResponseWriter, r *http.Request) {
	db, err := db.GetMongoDB()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	} else {
		bookModel := model.BookModel{
			Db:         db,
			Collection: "books",
		}
		vars := mux.Vars(r)
		id := vars["id"]
		book, err2 := bookModel.Find(id)
		if err2 != nil {
			respondWithError(w, http.StatusBadRequest, err2.Error())
			return
		} else {
			respondWithJson(w, http.StatusOK, book)
		}
	}
}

func FindAllBook(w http.ResponseWriter, r *http.Request) {
	db, err := db.GetMongoDB()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	} else {
		bookModel := model.BookModel{
			Db:         db,
			Collection: "books",
		}
		book, err2 := bookModel.FindAll()
		if err2 != nil {
			respondWithError(w, http.StatusBadRequest, err2.Error())
			return
		} else {
			respondWithJson(w, http.StatusOK, book)
		}
	}
}

func DeleteBook(w http.ResponseWriter, r *http.Request) {
	db, err := db.GetMongoDB()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	} else {
		bookModel := model.BookModel{
			Db:         db,
			Collection: "books",
		}
		vars := mux.Vars(r)
		id := vars["id"]
		book, _ := bookModel.Find(id)
		err2 := bookModel.Delete(book)
		if err2 != nil {
			respondWithError(w, http.StatusBadRequest, err2.Error())
			return
		} else {
			respondWithJson(w, http.StatusOK, book)
		}
	}
}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
