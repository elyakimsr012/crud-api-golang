package db

import (
	"gopkg.in/mgo.v2"
)

// func CreateDatabaseConnection(dbName string) (*mongo.Client, error) {
// 	clientOptions := options.Client().ApplyURI("mongodb+srv://elyakimmm:elyakim01@elcluster.gwk0h.mongodb.net/book-api?retryWrites=true&w=majority")

// 	// Connect to MongoDB
// 	client, err := mongo.Connect(context.TODO(), clientOptions)

// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	// Check the connection
// 	err = client.Ping(context.TODO(), nil)

// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	fmt.Println("Connected to MongoDB!")
// 	return client, err
// }

func GetMongoDB() (*mgo.Database, error) {
	hosts := "localhost:27017"
	dbName := "book-api"

	session, err := mgo.Dial(hosts)
	if err != nil {
		return nil, err
	}
	db := session.DB(dbName)

	return db, nil
}

// func getSession() *mgo.Session {
// 	s, err := mgo.Dial("elcluster-shard-00-01.gwk0h.mongodb.net:27017")
// 	if err != nil {
// 		panic(err)
// 	}
// 	return s
// }
