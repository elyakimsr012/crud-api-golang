package model

import (
	"github.com/elyakimnf/books-api/book"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type BookModel struct {
	Db         *mgo.Database
	Collection string
}

func (bookModel BookModel) FindAll() (book []book.Book, err error) {
	err = bookModel.Db.C(bookModel.Collection).Find(bson.M{}).All(&book)
	return
}

func (bookModel BookModel) Find(id string) (book book.Book, err error) {
	err = bookModel.Db.C(bookModel.Collection).FindId(bson.ObjectIdHex(id)).One(&book)
	return
}

func (bookModel BookModel) Create(book *book.Book) error {
	err := bookModel.Db.C(bookModel.Collection).Insert(&book)
	return err
}

func (bookModel BookModel) Update(book *book.Book) error {
	err := bookModel.Db.C(bookModel.Collection).UpdateId(book.Id, &book)
	return err
}

func (bookModel BookModel) Delete(book book.Book) error {
	err := bookModel.Db.C(bookModel.Collection).Remove(book)
	return err
}
